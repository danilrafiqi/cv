import React, { Component } from 'react';
import './App.css';
import Content from './components/Content';
import Loader from './components/Loader';


class App extends Component {
  render() {

    return (
      <main style={{
      	height: '100vh',
      	width: '100vw',
      	overflow: 'hidden',
      	position: 'absolute',
      	top: '0px'
      }}>
		<Loader/>
        <Content/>
      </main>
    );
  }
}








export default App;
