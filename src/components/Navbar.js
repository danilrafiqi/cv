import React from 'react';
import {Navbar, NavItem} from 'react-materialize';

const Navbars = (props) => {
  return (
	<Navbar brand='danilrafiqi' right className='cyan darken-4' options={ {edge:'right'}}>
	  <NavItem href=''>Welcome</NavItem>
	  <NavItem >About Me</NavItem>
	  <NavItem href=''>Portofolio</NavItem>
	  <NavItem href=''>Skill</NavItem>
	</Navbar>
  )
}

export default Navbars;