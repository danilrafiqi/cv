import React from 'react';
import {Row, Col} from 'react-materialize';
import About from './contents/About';
import Navbars from './Navbar';

const Content = (props) => {
  return (
    <Row style={{
        height: '100%', display: 'flex'}}>
			<Col s={12} m={12} l={4} xl={4} className='cyan darken-4'>
				<About/>
			</Col>
			<Col s={12} m={12} l={8} xl={8} className='cyan darken-3'>

				<Navbars/>
			</Col>
    </Row>
  )
}

export default Content;
