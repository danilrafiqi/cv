import React from 'react';
import { RingLoader } from 'react-spinners';
 
class Loader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }



  // componentDidMount(){
  //   setTimeout(
  //     ()=>{
  //       this.setState({
  //         loading: false
  //       })
  //     }, 4000
  //   )
  // }

  render() {
    return (
      <div className='sweet-loading'>
        <RingLoader
          color={'white'} 
          sizeUnit={"px"}
          size={150}
          loading={this.state.loading} 
          loaderStyle={{display: "block", margin: "0 auto", position: 'absolute !important', top: '50%',left: '50%'}}
        />
      </div>
    )
  }
}

export default Loader